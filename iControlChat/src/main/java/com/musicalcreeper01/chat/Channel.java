package com.musicalcreeper01.chat;

import java.util.ArrayList;
import java.util.List;





import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*
 * @AUTHOR: Keith 'MusicalCreeper01' M
 */
public class Channel {

	private List<Player> members = new ArrayList<Player>();
	private Player Owner;
	
	private List<Player> admins = new ArrayList<Player>();
	
	public String name;
	
	public boolean Public = true;
	public boolean SilenceMembers = false;
	
	public Channel(Player o){
		name = o.getName();
		setOwner(o);
	}
	
	public void Close(Player closer){
		for(Player m : members){
			m.sendMessage(closer.getDisplayName() + ChatColor.GOLD + "" + ChatColor.BOLD + " has closed the channel " + name);
			m.playSound(m.getLocation(), Sound.ANVIL_BREAK, 1, 1);
		}
		members = null;
		Owner = null;
		admins = null;
		iControlChat.channels.remove(this);
	}

	public void addMember (Player player){
		members.add(player);
		if(Owner != player)
			player.sendMessage(ChatColor.GOLD + "You are now a member of the channel \"" + name + "\"");
	}
	
	public void addAdmin(Player player){
		admins.add(player);
		player.sendMessage(ChatColor.GOLD + "You are now a channel admin");
	}
	
	public void removePlayer(Player p){
		if(members.contains(p))
			members.remove(p);
		if(admins.contains(p))
			admins.remove(p);
		
		for(Player m : members){
			m.sendMessage(p.getDisplayName() + ChatColor.AQUA + " has left the channel " + name);
		}
	}
	
	public List<Player> getMembers() {
		return members;
	}

	public List<String> getMembersNames() {
		List<String> l = new ArrayList<String>();
		for(Player p : members){
			l.add(p.getName());
		}
		return l;
	}
	
	public void setMembers(List<Player> members) {
		this.members = members;
	}

	public Player getOwner() {
		return Owner;
	}

	public void setOwner(Player owner) {
		Owner = owner;
		owner.sendMessage(ChatColor.AQUA + "You are now the owner of the channel \"" + name + "\"");
	}

	public List<Player> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Player> admins) {
		this.admins = admins;
	}
	
	
}
