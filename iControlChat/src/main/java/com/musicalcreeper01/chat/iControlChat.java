package com.musicalcreeper01.chat;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

public final class iControlChat extends JavaPlugin implements Listener {

	public class ChannelRequest {
		public Channel channel;
		public Player player;

		public ChannelRequest(Channel c, Player p) {
			channel = c;
			player = p;
		}
	}
	
	public List<Player> bypass = new ArrayList<Player>();

	public static List<Channel> channels = new ArrayList<Channel>();

	public static List<ChannelRequest> requests = new ArrayList<ChannelRequest>();

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}

	public Inventory createChatOptions(Player p) {
		Inventory chatSettingsInventory = Bukkit.createInventory(null, 9,
				"Chat Settings");

		chatSettingsInventory.setItem(0, new ItemStack(Material.EMERALD_BLOCK));
		ItemMeta tempMeta = chatSettingsInventory.getItem(0).getItemMeta();
		tempMeta.setDisplayName("Create Channel");
		chatSettingsInventory.getItem(0).setItemMeta(tempMeta);

		if (playerInChannel(p)) {
			chatSettingsInventory.setItem(1, new ItemStack(
					Material.REDSTONE_BLOCK));
			tempMeta = chatSettingsInventory.getItem(1).getItemMeta();
			if (isOwner(p))
				tempMeta.setDisplayName("Close Channel \"" + getChannel(p).name
						+ "\"");
			else
				tempMeta.setDisplayName("Leave Channel \"" + getChannel(p).name
						+ "\"");
			chatSettingsInventory.getItem(1).setItemMeta(tempMeta);
		} else if (channels.size() > 0 && !isOwner(p)) {
			chatSettingsInventory.setItem(1, new ItemStack(Material.PAPER));
			tempMeta = chatSettingsInventory.getItem(1).getItemMeta();
			tempMeta.setDisplayName("Channel List");
			chatSettingsInventory.getItem(1).setItemMeta(tempMeta);
		}

		if (isOwner(p)) {
			chatSettingsInventory
					.setItem(3, new ItemStack(Material.GOLD_BLOCK));
			tempMeta = chatSettingsInventory.getItem(3).getItemMeta();
			tempMeta.setDisplayName("Promote Player");
			chatSettingsInventory.getItem(3).setItemMeta(tempMeta);

			chatSettingsInventory
					.setItem(4, new ItemStack(Material.IRON_BLOCK));
			tempMeta = chatSettingsInventory.getItem(4).getItemMeta();
			tempMeta.setDisplayName("Demote Admin");
			chatSettingsInventory.getItem(4).setItemMeta(tempMeta);
		}
		return chatSettingsInventory;
	}

	public Inventory getChannelList(Player p) {

		Inventory channelList = Bukkit.createInventory(null, 9, "Channels");

		if (channels.size() <= 9) {
			channelList = Bukkit.createInventory(null, 9, "Channels");
		} else if (channels.size() > 9 && channels.size() <= 18) {
			channelList = Bukkit.createInventory(null, 18, "Channels");
		} else {
			channelList = Bukkit.createInventory(null, 27, "Channels");
		}
		int i = 0;
		for (Channel c : channels) {
			if (!c.getMembers().contains(p)) {
				channelList.setItem(i, new ItemStack(Material.SKULL_ITEM));
				channelList.getItem(i).setDurability((short) 3);

				ItemMeta im = channelList.getItem(i).getItemMeta();
				im.setDisplayName(c.name);
				im.setLore(c.getMembersNames());
				channelList.getItem(i).setItemMeta(im);

				SkullMeta meta = (SkullMeta) channelList.getItem(i)
						.getItemMeta();
				meta.setOwner(c.getOwner().getName());

				++i;
			}
		}

		return channelList;
	}

	public Inventory openAdminPromotion(Channel c) {
		Inventory inv = Bukkit.createInventory(null, 9, "Promote To Admin");

		if (c.getMembers().size() <= 9) {
			inv = Bukkit.createInventory(null, 9, "Promote To Admin");
		} else if (c.getMembers().size() > 9 && channels.size() <= 18) {
			inv = Bukkit.createInventory(null, 18, "Promote To Admin");
		} else {
			inv = Bukkit.createInventory(null, 27, "Promote To Admin");
		}

		int i = 0;
		for (Player p : c.getMembers()) {
			if (!c.getAdmins().contains(p) && c.getOwner() != p) {
				inv.setItem(i, new ItemStack(Material.SKULL_ITEM));
				inv.getItem(i).setDurability((short) 3);

				ItemMeta im = inv.getItem(i).getItemMeta();
				im.setDisplayName(p.getName());
				List<String> lore = new ArrayList<String>();
				lore.add(c.name);
				im.setLore(lore);
				inv.getItem(i).setItemMeta(im);

				SkullMeta meta = (SkullMeta) inv.getItem(i).getItemMeta();
				meta.setOwner(c.getOwner().getName());

				++i;
			}
		}

		return inv;
	}

	public Inventory openAdminDemotion(Channel c) {
		Inventory inv = Bukkit.createInventory(null, 9, "Demote");

		if (c.getMembers().size() <= 9) {
			inv = Bukkit.createInventory(null, 9, "Demote");
		} else if (c.getMembers().size() > 9 && channels.size() <= 18) {
			inv = Bukkit.createInventory(null, 18, "Demote");
		} else {
			inv = Bukkit.createInventory(null, 27, "Demote");
		}

		int i = 0;
		for (Player p : c.getAdmins()) {

			inv.setItem(i, new ItemStack(Material.SKULL_ITEM));
			inv.getItem(i).setDurability((short) 3);

			ItemMeta im = inv.getItem(i).getItemMeta();
			im.setDisplayName(p.getName());
			List<String> lore = new ArrayList<String>();
			lore.add(c.name);
			im.setLore(lore);
			inv.getItem(i).setItemMeta(im);

			SkullMeta meta = (SkullMeta) inv.getItem(i).getItemMeta();
			meta.setOwner(c.getOwner().getName());

			++i;

		}

		return inv;
	}

	public void leaveChannel(Player player) {
		if (getChannel(player).getOwner() == player
				|| getChannel(player).getAdmins().contains(player)) {
			player.sendMessage(ChatColor.AQUA + "You have closed the channel "
					+ getChannel(player).name);
			getChannel(player).Close(player);
		} else if (getChannel(player).getOwner() != player
				&& !getChannel(player).getAdmins().contains(player)) {
			player.sendMessage(ChatColor.AQUA + "You have left the channel "
					+ getChannel(player).name);
			getChannel(player).removePlayer(player);
		}
		player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
	}

	public void createChannel(Player player) {
		if (playerInChannel(player)) {
			player.sendMessage(ChatColor.RED
					+ "To create a chat channel, you must first leave your current channel \"/c leave\"");
			player.closeInventory();

		} else {
			player.sendMessage(ChatColor.DARK_GREEN
					+ "Creating chat channel...");
			Channel c = new Channel(player);
			c.addMember(player);
			channels.add(c);
			player.closeInventory();
		}
		player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 1);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked(); // The player that
														// clicked the item
		ItemStack clicked = event.getCurrentItem(); // The item that was clicked
		Inventory inventory = event.getInventory(); // The inventory that was
		if (clicked != null && clicked.hasItemMeta()) {
			if (inventory.getName().equals("Chat Settings")) {
				if (clicked.getType() == Material.EMERALD_BLOCK) {
					createChannel(player);
					event.setCancelled(true);
				} else if (clicked.getType() == Material.REDSTONE_BLOCK) {
					player.closeInventory();
					leaveChannel(player);
					event.setCancelled(true);
				} else if (clicked.getType() == Material.PAPER) {
					player.closeInventory();
					player.sendMessage(ChatColor.AQUA
							+ "Getting channel list...");
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.openInventory(getChannelList(player));
				} else if (clicked.getItemMeta().getDisplayName()
						.equalsIgnoreCase("Promote Player")) {
					player.closeInventory();
					player.sendMessage(ChatColor.AQUA
							+ "Getting member list...");
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.openInventory(openAdminPromotion(getChannel(player)));
				} else if (clicked.getItemMeta().getDisplayName()
						.equalsIgnoreCase("Demote Admin")) {
					player.closeInventory();
					player.sendMessage(ChatColor.AQUA + "Getting admin list...");
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.openInventory(openAdminDemotion(getChannel(player)));
				}
				event.setCancelled(true);
			} else if (inventory.getName().equals("Channels")) {
				if (getChannelNames().contains(
						clicked.getItemMeta().getDisplayName())) {
					Channel channel = getChannel(clicked.getItemMeta()
							.getDisplayName());
					sendChannelRequest(channel, player);
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.closeInventory();
				}
				event.setCancelled(true);

			} else if (inventory.getName().equalsIgnoreCase("Promote To Admin")) {
				if (clicked.getType() == Material.SKULL_ITEM
						&& clicked.hasItemMeta()) {
					Channel channel = getChannel(clicked.getItemMeta()
							.getLore().get(0));

					PromoteAdmin(
							player,
							Bukkit.getServer().getPlayer(
									clicked.getItemMeta().getDisplayName()),
							channel);
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.closeInventory();
				}
				event.setCancelled(true);
			} else if (inventory.getName().equalsIgnoreCase("Demote")) {
				if (clicked.getType() == Material.SKULL_ITEM
						&& clicked.hasItemMeta()) {
					Channel channel = getChannel(clicked.getItemMeta()
							.getLore().get(0));
					DemoteAdmin(
							player,
							Bukkit.getServer().getPlayer(
									clicked.getItemMeta().getDisplayName()),
							channel);
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					player.closeInventory();
				}
				event.setCancelled(true);
			}
		}
	}

	public void PromoteAdmin(Player ex, Player p, Channel c) {
		getLogger().log(
				Level.INFO,
				ex.getName() + " is attempting to promote " + p.getName()
						+ " to admin in the channel " + c.name);
		if (c.getMembers().contains(p)) {
			c.getOwner().sendMessage(
					ChatColor.GOLD + "" + ChatColor.BOLD + "You have promoted "
							+ p.getDisplayName() + " to " + ChatColor.DARK_RED
							+ "Admin");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD
					+ "You have been promoted to " + ChatColor.DARK_RED
					+ "Admin" + ChatColor.GOLD + "" + ChatColor.BOLD
					+ " in the channel " + c.name);
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
			c.addAdmin(p);
			getLogger().log(
					Level.INFO,
					ex.getName() + " promoted " + p.getName()
							+ " to admin in the channel " + c.name);
		}

	}

	public void DemoteAdmin(Player ex, Player p, Channel c) {
		getLogger().log(
				Level.INFO,
				ex.getName() + " is attempting to demote " + p.getName()
						+ " to member in the channel " + c.name);
		if (c.getAdmins().contains(p)) {
			c.getOwner().sendMessage(
					ChatColor.GOLD + "" + ChatColor.BOLD + "You have demoted"
							+ p.getDisplayName() + ChatColor.GOLD + ""
							+ ChatColor.BOLD + " to " + ChatColor.AQUA
							+ "Member");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD
					+ "You have been demoted to " + ChatColor.DARK_RED
					+ "Member" + ChatColor.GOLD + "" + ChatColor.BOLD
					+ " in the channel " + c.name);
			p.playSound(p.getLocation(), Sound.ANVIL_LAND, 1, 1);
			c.getAdmins().remove(p);
			getLogger().log(
					Level.INFO,
					ex.getName() + " demoted " + p.getName()
							+ " to member in the channel " + c.name);
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItem();
		if (player.getItemInHand() != null) {
			if (item.getType() == Material.BOOK) {
				player.openInventory(createChatOptions(player));
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		player.getInventory().setItem(0, new ItemStack(Material.BOOK));
	}

	public void sendChannelRequest(String channel, Player player) {
		Channel c = getChannel(channel);
		sendChannelRequest(c, player);
	}

	public void sendChannelRequest(Channel c, Player player) {
		player.sendMessage(ChatColor.AQUA + "Sending channel request to "
				+ c.name);
		List<ChannelRequest> toremove = new ArrayList<ChannelRequest>();
		for (ChannelRequest r : requests) {
			if (r.channel == c)
				toremove.add(r);
		}
		for (ChannelRequest r : toremove) {
			requests.remove(r);
		}
		requests.add(new ChannelRequest(c, player));
		c.getOwner().sendMessage(
				player.getDisplayName() + ChatColor.GOLD + "" + ChatColor.BOLD
						+ " has requested to be allowed into your channel");
		c.getOwner().sendMessage(
				ChatColor.GOLD + "" + ChatColor.BOLD + "Type \""
						+ ChatColor.GREEN + "/c accept" + ChatColor.GOLD + ""
						+ ChatColor.BOLD + "\" to allow them to join");
		c.getOwner().sendMessage(
				ChatColor.GOLD + "" + ChatColor.BOLD + "Or \"" + ChatColor.RED
						+ "/c deny" + ChatColor.GOLD + "" + ChatColor.BOLD
						+ "\" to deny them to join");
		c.getOwner().playSound(c.getOwner().getLocation(), Sound.ORB_PICKUP, 1,
				1);
	}

	public List<String> getChannelNames() {
		List<String> names = new ArrayList<String>();

		for (Channel c : channels)
			names.add(c.name);

		return names;
	}

	public boolean playerInChannel(Player p) {
		boolean isChannelOwner = false;
		for (Channel c : channels) {
			if (c.getOwner() == p || c.getMembers().contains(p)) {
				isChannelOwner = true;
			}
		}
		return isChannelOwner;
	}

	public Channel getChannel(Player p) {
		Channel channel = null;
		if (playerInChannel(p)) {
			for (Channel c : channels) {
				if (c.getMembers().contains(p)) {
					channel = c;
				}
			}
		}
		return channel;
	}

	public Channel getChannel(String s) {
		Channel channel = null;
		for (Channel c : channels) {
			if (c.name.equalsIgnoreCase(s)) {
				channel = c;
			}
		}
		return channel;
	}

	public boolean isOwner(Player p) {
		for (Channel c : channels) {
			if (c.getOwner() == p) {
				return true;
			}
		}
		return false;
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (event.getMessage().startsWith("!")
				&& player.hasPermission("worldchat.admin")) {
			event.getRecipients().clear();
			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
				if (p.hasPermission("worldchat.admin")) {
					event.getRecipients().add(p);

				}
			}
			event.setMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD
					+ "Staff Chat: " + event.getMessage().substring(1));
			
			if(Bukkit.getServer().getOnlinePlayers().size() == 1){
				player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "No one is online!");
			}
		}else if (event.getMessage().startsWith("!")
				&& !player.hasPermission("worldchat.admin")){
			
			event.getRecipients().clear();
			player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You are not allowed to use staff chat!");
			
		}else if (event.getMessage().startsWith("@")) {
			if (playerInChannel(player)) {
				event.getRecipients().clear();
				if ((getChannel(player).SilenceMembers
						&& (getChannel(player).getAdmins().contains(player) || getChannel(
								player).getOwner() == player) || (!getChannel(player).SilenceMembers))) {
					for (Player p : getChannel(player).getMembers()) {
						event.getRecipients().add(p);
					}
				}
				event.setMessage(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD
						+  "Channel Chat: " + event.getMessage().substring(1));
			}else{
				event.getRecipients().clear();
				player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You are not in a channel, do '/c gui' to create one");
			}
		}else {
			if(bypass.contains(player)){
				event.getRecipients().clear();;
			}
			for(Player p : Bukkit.getServer().getOnlinePlayers()){
				if(bypass.contains(player)){
					event.getRecipients().add(p);
				}else if(p.getWorld() != player.getWorld() && event.getRecipients().contains(p))
					event.getRecipients().remove(p);
			}
			if(player.getWorld().getPlayers().size() == 1){
				player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "No one can hear you!");
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("c")
				|| cmd.getName().equalsIgnoreCase("chat")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("This command can only be run by a player.");
			} else {
				Player player = (Player) sender;
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("accept")) {
						if (requests.size() > 0) {
							List<ChannelRequest> toRemove = new ArrayList<ChannelRequest>();
							for (ChannelRequest c : requests) {
								if (c.channel.getOwner() == player
										|| c.channel.getAdmins().contains(
												player)) {
									c.channel.addMember(c.player);
									player.sendMessage(ChatColor.GOLD + ""
											+ ChatColor.BOLD + "Added "
											+ c.player.getDisplayName()
											+ ChatColor.GOLD + ""
											+ ChatColor.BOLD
											+ " to the channel");
									c.player.playSound(c.player.getLocation(),
											Sound.ORB_PICKUP, 1, 1);
									toRemove.add(c);
								}
							}
							for (ChannelRequest c : toRemove)
								requests.remove(c);
						} else {
							player.sendMessage(ChatColor.RED + ""
									+ ChatColor.BOLD + "No pending requests");
						}
					} else if (args[0].equalsIgnoreCase("deny")) {
						if (requests.size() > 0) {
							List<ChannelRequest> toRemove = new ArrayList<ChannelRequest>();
							for (ChannelRequest c : requests) {
								if (c.channel.getOwner() == player
										|| c.channel.getAdmins().contains(
												player)) {
									player.sendMessage(ChatColor.GOLD + ""
											+ ChatColor.BOLD + "Denyed "
											+ c.player.getDisplayName()
											+ ChatColor.GOLD + ""
											+ ChatColor.BOLD
											+ "'s request to join the channel");
									c.player.sendMessage(ChatColor.GOLD + ""
											+ ChatColor.BOLD
											+ "Your request to join \""
											+ c.channel.name
											+ "\" has been denyed");
									c.player.playSound(c.player.getLocation(),
											Sound.ANVIL_LAND, 1, 1);
									toRemove.add(c);
								}
							}
							for (ChannelRequest c : toRemove)
								requests.remove(c);
						} else {
							player.sendMessage(ChatColor.RED + ""
									+ ChatColor.BOLD + "No pending requests");
						}
					} else if (args[0].equalsIgnoreCase("leave")) {
						leaveChannel(player);
					} else if (args[0].equalsIgnoreCase("create")) {
						createChannel(player);
					} else if (args[0].equalsIgnoreCase("gui")) {
						player.openInventory(createChatOptions(player));
					}else if (args[0].equalsIgnoreCase("bypass")) {
						bypass.add(player);
					}
				}
			}
			return true;
		}
		return false;
	}
}